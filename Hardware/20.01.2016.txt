#Virtualizační technologie
 - Dnes se stále více používají virtuální technologie, umožňující provozovat v hostitelském operačním systému tzv. virtuální stroje.
 - Na hardwaru jednoho počítače tak můžeme provozovat několik virtuálních počítačů
 - testování a ladění programů bez nutnosti instalovat testovací verze operačního systému
 - prezentaci a výuku, kdy na jednom počítači můžeme předvádět několik operačních systémů zároveň simulaci provozních stavů
 - konsolidaci serverů bez nutnosti kupovat další hardware
 - protože se virtuální počítače používají stále více tak výrobci CPU přišli s HW podporou virtualizace
 - intel zavádí postupně u nových procesorů technologii původně označovanou Vanderpool, jejiž oficiální název je Intel Virtualisation Technology
 - AMD vyvíjela virtualizační technologii pod názbem Pacifica, její oficiální název zní AMD Virtualization

#Vnitřní frekvence (takt)

 - Elektronické obvody fyzicky tvořící mikroprocesor potrřebují taktovací impulsy, které určují jejich "pracovní tempo"
 - Každá základní deska je vybavena generátorem taktů, generujícím taktovací impulsy pro mikroprocesor
 - Z této externí frekvence je odvozena vnitřní frekvence mikroprocesoru
 - mezi externí sběrnicí a mikroprocesorem pracuje tzv. násobička,
 - která převádí pomalejší externí takt na vyšší interní frekvenci mikroprocesoru
 - Mikroprocesory mají poměr obou frekvencí určen pevně a měnit jej nelze.
 - Násobička je součástí mikroprocesoru, proto pokud vstadíme do základní desky nový procesor, nastaví se správný poměr frekvencí automaticky
 - Postupně uvidíme, že spolupráce základní desky a mikroprocesoru je závislá na více faktorech - patici, frekvenci USB, napájecím napětí, zdroji...
 - Ne každý procesor se tedy "dohodne" s každou základní deskou
 - Dříve se zvyšoval výkon mikroprocesoru nárůstem vnitřní frekvence, ale pod 4 GHz bylo dosaženo maxima.
 - Dnes se výkon zvyšuje paralelním zpravováním instrukcí ve více jádrech
#Vnější frekvence
 - List item
